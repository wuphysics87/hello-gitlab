<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Hello Gitlab](#hello-gitlab)
- [Asymmetric Cryptography With SSH Keys](#asymmetric-cryptography-with-ssh-keys)
- [Automating Our Setup](#automating-our-setup)
- [Configuring Git Lab](#configuring-git-lab)
- [Forking And Pushing To GitLab](#forking-and-pushing-to-gitlab)

<!-- markdown-toc end -->

# Hello Gitlab

In [Hello Bitwarden](https://gitlab.com/wuphysics87/hello-bitwarden), we set up a Bitwarden account, the Bitwarden browser extension, and created a Gitlab account. If you haven't done so already, go through that process in [Hello Bitwarden](https://gitlab.com/wuphysics87/hello-bitwarden)

This repo, which is hosted on [GitLab](https://gitlab.com), is dedicated to learning how to create your own GitLab repositories. We will go through the process of configuring your computer to remotely create GitLab repos, fork other git repos from GitLab, and finally push your repos up to GitLab. All from the comfort of your local terminal.


If you haven't already, clone this repo to your computer using **Codium** or `git` through your CLI.

Let's get started!

# Asymmetric Cryptography With SSH Keys

If you want to push code up to GitLab, you need to prove who you are. By default, this involves entering your user name and password whenever you run `git push`. Considering we made a strong random password for our account, this would be cumbersome. Luckily, there are other ways to authenticate. The standard way is through [asymmetric or public key cryptography](https://www.techtarget.com/searchsecurity/definition/asymmetric-cryptography). We'll be running a script to automate all of this, but in practice, the way you would do this is to:

1. Open a Terminal
2. Run `ssh-keygen`
3. Modifying `~/.ssh/config` so our **`ssh`** command knows where to find our key
4. Distribute our public key 

If this is the type of thing you'd do often, it's worth learning the manual process. We won't always be so lucky as to have a pre-written script to automate this, and learning the fundamentals of `ssh` will enable you to do [many other things](https://en.wikipedia.org/wiki/Secure_Shell).

# Automating Our Setup

If you have this repo open in your text editor or terminal, you'll notice the script directory. There, you'll see the `ssh-config` and `gl` scripts. Take a peek inside `ssh-config`. Based off of the last section, you should have some idea of what it's doing. Once you've looked at it, on MacOS run:

``` sh
bash scripts/ssh-config
```

or on Windows

``` sh
hsh scripts/ssh-config
```

to set up our SSH configuration.

# Configuring Git Lab

`ssh-config` just automated the first 3 steps listed of the cryptography section. And it set us up to complete step 4 by spitting out our public key. To distribute our public key to GitLab:

1. Copy the public key from the terminal
![ssh pub](images/ssh-pub.png)

2. Log into your GitLab account in your browser
3. Click on your avatar in the top right corner

![gl login](images/gl-login.png)

4. Click on 'Preferences'
5. On the left panel, click on 'SSH Keys'

![gl ssh](images/gl-ssh.png)

6. Paste your public key in the 'Key' field and click 'Add key'
7. Finally, return to your terminal and type `ssh git@gitlab.com`, type 'yes' when prompted

![ ssh trust ](images/ssh-trust.png)

If everything went according to plan, you'll see:

![ ssh success ](images/ssh-success.png)

# Forking And Pushing To GitLab

**As of this commit, the gl script does not work pushing so it will need to be done manually**

[If You Don't Like It Fork It!](https://www.zdnet.com/article/if-you-dont-like-it-fork-it/)

When someone creates a public repository you want to work with, we clone that repository. But what happens after that? Do we send our changes back? Do we keep working on our own? 

When we continue to work on our own, it's called forking. We'll want to learn how to fork others' repositories and push them up to our own GitLab account.  Luckily, there's a script for that!

From the mac users, run:

``` sh
cp scripts/gl ~/.local/bin
```

for windows users
``` sh
cd scripts && dos2unix gl && cp ~/.local/bin
```

Then refresh your terminal by running `zsh`.

You will now be able to run the `gl` command (short for GitLab). This is a special command serving two purposes. 

Its first is to turn an ordinary directory into a git repository and then push it up to your GitLab account. 

Try this by running `cd` followed by, `mkdir ~/my-cool-repo`, `cd ~/my-cool-repo`, and finally `gl`. The script will prompt to choose a license. For now pick whichever you want. Free and Open Source Licenses are a topic for another time. 

Once you've choosen one, the script will finish by pushing your new repo up to GitLab.  You'll find it at `https://gitlab.com/USERNAME/my-cool-repo` You'll see the repo has the license you choose as well as an empty README. Pretty handy.

The second purpose of `gl` is for forking repositories. We just tested it in an empty directory.  See what happens when run it from this repository.

# Making A Repository Public

By default when you create a repository on GitLab it is not publicly viewable. In order to make it public:

1. From the repo webpage, click on the gear on the bottom of the left panel and go to settings

![ gitlab settings ](images/gitlab-settings.png)

2. Navigate to 'Visibility, project features, permissions' and expand it

![ gitlab settings ](images/gitlab-public.png)

3. Under the 'Project visibility' subsection, change 'Private' to 'Public'

4. Scroll down to just before the 'Badges' subsection and click 'Save Changes'

![ gitlab settings ](images/gitlab-settings.png)

You will now be able to share your repo with anyone in the world to view on GitLab.com with a link like:

``` sh
https://gitlab.com/GITLAB-USERNAME/GITLAB-REPO
```

Where *GITLAB-USERNAME* is your GitLab Username. And *GITLAB-REPO* is the name of this repository.

Not only can anyone see this repository, they will be able to `clone` via https by running:

``` sh
git clone https://gitlab.com/GITLAB-USERNAME/GITLAB-REPO
```

And anyone like you with `ssh` credentials enabled for this repository can clone it with:

``` sh
git clone git@gitlab.com:GITLAB-USERNAME/GITLAB-REPO
```

As we've seen before, the advantage of using `ssh keys` is not only is it more secure, you also won't be prompted for a username and password when you run

``` sh
git push origin main
```

from the repo's root directory.  Very cool!
